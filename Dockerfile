ARG  IMAGE_TAG=changeme
FROM goacme/lego:${IMAGE_TAG}

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
