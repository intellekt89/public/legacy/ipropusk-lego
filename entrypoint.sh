#!/bin/sh

trap exit TERM

sleep ${START_DELAY}
while true; do 
  ./lego --dns ${DNS} --domains ${LEGO_CERT_DOMAIN} --email ${LEGO_ACCOUNT_EMAIL} --dns.resolvers ${DNS_RESOLVERS} renew; 
  sleep ${RENEW_INTERVAL}; 
done;