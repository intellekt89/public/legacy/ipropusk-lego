# ipropusk-lego

## Environment variables, default values for Selectel

- `START_DELAY` 24h
- `RENEW_INTERVAL` 24h
- `DNS` selectel
- `DNS_RESOLVERS` ns1.selectel.ru,ns2.selectel.ru,ns3.selectel.ru,ns4.selectel.ru
- `LEGO_CERT_DOMAIN` *.example.com
- `LEGO_ACCOUNT_EMAIL` admin@example.com
- `SELECTEL_API_TOKEN` https://my.selectel.ru/profile/apikeys/
- `LEGO_DISABLE_CNAME_SUPPORT` true

## Test

```bash
docker run -it --rm \
  --env START_DELAY=300 \
  --env RENEW_INTERVAL=300 \
  --env LEGO_DISABLE_CNAME_SUPPORT=true \
  --env LEGO_ACCOUNT_EMAIL=admin@example.com \
  --env LEGO_CERT_DOMAIN=*.example.com \
  --env SELECTEL_API_TOKEN=xxxxx \
  --env DNS=selectel \
  --env DNS_RESOLVERS=ns1.selectel.ru,ns2.selectel.ru,ns3.selectel.ru,ns4.selectel.ru \
  --entrypoint "sh" \
  registry.gitlab.com/intellekt89/public/legacy/ipropusk-lego:v4.14.2

./lego --server=https://acme-staging-v02.api.letsencrypt.org/directory --dns ${DNS} --domains ${LEGO_CERT_DOMAIN} --email ${LEGO_ACCOUNT_EMAIL} --dns.resolvers ${DNS_RESOLVERS} --accept-tos run

./lego --server=https://acme-staging-v02.api.letsencrypt.org/directory --dns ${DNS} --domains ${LEGO_CERT_DOMAIN} --email ${LEGO_ACCOUNT_EMAIL} --dns.resolvers ${DNS_RESOLVERS} --accept-tos renew
```

## New cert

```bash
docker exec -it i-propusk_lego-ticketdocs_1 sh

./lego --dns ${DNS} --domains ${LEGO_CERT_DOMAIN} --email ${LEGO_ACCOUNT_EMAIL} --dns.resolvers ${DNS_RESOLVERS} --accept-tos run

ls -1 /.lego/certificates/
```